# -*- coding: utf-8 -*-
from django.http import JsonResponse
from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from qr_upload.qr_upload.models import Document
from qr_upload.qr_upload.forms import DocumentForm
from django.views.decorators.csrf import csrf_exempt

from subprocess import PIPE, Popen
import re

@csrf_exempt
def list(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile=request.FILES['docfile'])
            newdoc.save()

            #for h in request.META:
            #    print('LOG: %s' % (h))
            fmt = request.META.get('HTTP_FORMAT', '')

            if fmt:
                return JsonResponse(scan(newdoc.docfile.file.name))

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('list'))
        else:
            print('LOG: FORM_INVALID: %s' % (form.errors))
    else:
        form = DocumentForm()  # A empty, unbound form

    documents = Document.objects.all()

    return render(
        request,
        'list.html',
        {'documents': documents, 'form': form}
    )

def scan(image_file):
    print('LOG: IMAGE_FILE: %s' % ( image_file))
    process = Popen(['zbarimg', image_file], stdout=PIPE, stderr=PIPE)
    stdout, stderr = process.communicate()
    outs = stdout.decode('utf-8') 
    #print('LOG: %s' % ( outs ))

    outs = stdout.decode('utf-8').split('\n')
    ret = {}
    idx = 0
    idx_str = ''
    qr_found = False

    for out in outs:
        if 'QR-Code' in out:
            idx_str = 'code' + str(idx)
            print('*** LOG: %s' % ( out ))
            ret[idx_str] = {}
            ret[idx_str]['content'] = out
            qr_found = True
        elif 'POSITION' in out:
            print('+++ LOG: POSITION: %s' % ( out ))
            try:
                found = re.search('IMAGE POSITION x: (\d+) y: (\d+) OUT', out)
            except AttributeError as e:
                found = None
                print('eee LOG: ERROR: %s' % ( e ))
            except Exception as e:
                found = None
                print('eee LOG: ERROR: %s' % ( e ))
            if found:
                if not idx_str in ret:
                    ret[idx_str] = {}
                ret[idx_str]['x'] = found.group(1)
                ret[idx_str]['y'] = found.group(2)
        else:
            print('LOG: %s' % ( out ))

        if qr_found == True:
            idx = idx + 1
        qr_found = False
    errs = stderr.decode('utf-8').split('\n')

    for out in errs:
        print('STDERR LOG: %s' % ( out ))
    return ret

