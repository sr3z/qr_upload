# -*- coding: utf-8 -*-
from django.conf.urls import url
from qr_upload.qr_upload.views import list

urlpatterns = [
    url(r'^list/$', list, name='list')
]
