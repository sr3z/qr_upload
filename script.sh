curl -v -c cookies.txt -b cookies.txt -X GET http://localhost:8000/qr_upload/list/

COOKIE='unknown'
unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
   #COOKIE=`grep -P -o "token=[a-zA-Z0-9]*" cookies.txt`
   COOKIE=`cat cookies.txt | grep csrftoken | cut -f 7`
elif [[ "$unamestr" == 'Darwin' ]]; then
   COOKIE=`cat cookies.txt | grep csrftoken | cut -f 7`
fi

echo "COOKIE: "$COOKIE
echo
IMG=/home/srez/Downloads/qr.png

curl -H 'Format: json' -v -c cookies.txt -b cookies.txt -F 'docfile=@'${IMG} http://localhost:8000/qr_upload/list/
#curl -v -c cookies.txt -b cookies.txt -F 'docfile=@'${IMG} http://localhost:8000/qr_upload/list/
